#!/usr/bin/env bash

files=$(find . -path './src/*'  -name '*.tf' -type f)

for f in $files
do
  if [[ -e "$f" ]]; then
    ./tools/terraform fmt "$f" && echo "$f"
  fi
done
