include {
  path = find_in_parent_folders()
}

locals {
  region = read_terragrunt_config(find_in_parent_folders()).locals.region
}

terraform {
  source = "../../..//src/vpc"
  extra_arguments "retry_lock" {
    commands = get_terraform_commands_that_need_locking()
    arguments = [
      "-lock-timeout=60m"
    ]
  }
  extra_arguments "secrets" {
    commands           = get_terraform_commands_that_need_vars()
    optional_var_files = ["${get_terragrunt_dir()}/secrets.tfvars"]
  }
}

inputs = {
  vpc_cidr             = "10.140.0.0/16"
  public_subnet_cidrs  = ["10.140.0.0/24"]
  private_subnet_cidrs = ["10.140.16.0/24", "10.140.17.0/24"]
  availability_zones   = ["${local.region}a", "${local.region}b"]
}