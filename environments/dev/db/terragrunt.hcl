include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../vpc", "../web-server"]
}

terraform {
  source = "../../..//src/db"
  extra_arguments "retry_lock" {
    commands = get_terraform_commands_that_need_locking()
    arguments = [
      "-lock-timeout=60m"
    ]
  }
  extra_arguments "secrets" {
    commands           = get_terraform_commands_that_need_vars()
    optional_var_files = ["${get_terragrunt_dir()}/secrets.tfvars"]
  }
}