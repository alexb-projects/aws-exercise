locals {
  environment = "prod"
  region      = "us-west-1"
}

remote_state {
  backend = "s3"
  config = {
    bucket  = "aws-excersise-project-alex-b-terraform-state"
    key     = "${local.environment}/${path_relative_to_include()}/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true

    dynamodb_table = "terraform_state_lock_table"
  }
}

inputs = {
  region      = local.region
  environment = local.environment
}