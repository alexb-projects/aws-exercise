# aws-exercise
Architecture Overview:

Current repository contains the code to provision a VPC with 1 public and 2 private subnets, an EC2 instance with the NGINX web server and an RDS PostgreSQL instance. The EC2 instance is publicly accessible and has an additional EBS volume mounted to /data-volume location. The DB instance is only accessible from the EC2 instance. Deployment is done using the terragrunt. The current configuration allows to deploy the same infrastructure to 2 different environments: dev and prod. Dev environment is provisioned in us-east-1 and the prod is provisioned in us-west-1.


Deployment instructions:

Assuming the AWS profile is configured, to create the infrastructure in the dev environment:
  1. From the root of the repository execute the following command: PATH=$PATH:"$(pwd)/tools"
  2. Navigate to ./environments/dev: cd environments/dev
  3. Execute: terragrunt run-all init
  4. Execute: terragrunt run-all apply --terragrunt-non-interactive -auto-approve

To create the infrastructure in the prod environment navigate to ./environments/prod directory and repeat steps 3 and 4

To view the greeting page served by the web-server EC2 instance login to the AWS console and retrieve the Public IPv4 DNS name of the instance. Access the page in the browser as follows:
http://public-ipv4-dns. Note that you should use the http schema.


To connect to the DB instance:
  1. Note the endpoint of the created postgresql-db instance.
  2. Use EC2 Instance Connect to the EC2 instance.
  3. Execute: psql -h <db-endpoint> -d DemoPostgresql -U administrator -W
  4. Enter the following password: 9ekzn$#z6zybsTGu
  5. Once the connection is established other command could be used like for example \du command to list the users and \l to list the available DBs.
  
To destroy the infrastructure use replace apply with the destroy command.
Ex.: terragrunt run-all destroy --terragrunt-non-interactive -auto-approve

 