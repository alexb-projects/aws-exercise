terraform {
  required_version = "~> 1.3.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.32.0"
    }
  }

  backend "s3" {
  }
}

provider "aws" {
  region = var.region
}

locals {
  resource_name = "postgresql-db"
  port          = 5432

  tags = {
    name  = local.resource_name
    env   = var.environment
    scope = "${var.environment}-${local.resource_name}"
  }
}

resource "aws_security_group" "this" {
  name        = "${local.resource_name}-sg"
  description = "Allow inbound / outbound traffic to the db"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "this" {
  description              = "Allow web-server to communicate with the db"
  type                     = "ingress"
  protocol                 = "tcp"
  source_security_group_id = data.terraform_remote_state.web_server.outputs.security_group_id
  from_port                = local.port
  to_port                  = local.port
  security_group_id        = aws_security_group.this.id
}

module "postgres_db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "5.1.0"

  identifier           = local.resource_name
  engine               = "postgres"
  engine_version       = "14.1"
  family               = "postgres14"
  major_engine_version = "14"
  instance_class       = "db.t4g.small"
  allocated_storage    = 10

  db_name                = "DemoPostgresql"
  username               = "administrator"
  create_random_password = false
  password               = "9ekzn$#z6zybsTGu"
  port                   = local.port

  db_subnet_group_name   = data.terraform_remote_state.vpc.outputs.db_subnet_group
  subnet_ids             = data.terraform_remote_state.vpc.outputs.subnets.private
  vpc_security_group_ids = [aws_security_group.this.id]

  multi_az                    = false
  create_cloudwatch_log_group = false
  backup_retention_period     = 1
  skip_final_snapshot         = true
  deletion_protection         = false

  maintenance_window              = "Mon:00:00-Mon:03:00"
  backup_window                   = "03:00-06:00"
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  tags = local.tags
}