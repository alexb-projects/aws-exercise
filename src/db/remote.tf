data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket  = "aws-excersise-project-alex-b-terraform-state"
    key     = "${var.environment}/vpc/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

data "terraform_remote_state" "web_server" {
  backend = "s3"

  config = {
    bucket  = "aws-excersise-project-alex-b-terraform-state"
    key     = "${var.environment}/web-server/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}