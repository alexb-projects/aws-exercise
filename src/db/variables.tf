variable "region" {
  type        = string
  description = "Region where the AWS resources are provisioned"
}

variable "environment" {
  type        = string
  description = "Environment: Prod or Dev"
}