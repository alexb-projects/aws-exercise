output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = module.postgres_db.db_instance_endpoint
}

output "db_instance_name" {
  description = "The database name"
  value       = module.postgres_db.db_instance_name
}