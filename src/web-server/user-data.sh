#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install nginx1 -y

# overwrite default index.html page
cat >/usr/share/nginx/html/index.html << EOF
<!DOCTYPE html>
<html>
  <style>
    h1 {
      text-align: center;
      font-size:60px;
      color:#590924
    }
    body {
      background-color: #092a5e;
    }
  </style>
  <body>
    <h1>${greeting_message}</h1>
  </body>
</html>
EOF

sudo systemctl enable nginx
sudo systemctl start nginx

# format and mount the 2nd provisioned ebs volume
sudo mkfs -t xfs /dev/xvdb
sudo mkdir /data-volume
sudo mount /dev/xvdb /data-volume
sudo chmod 777 /data-volume/

#install postgresql to test the DB connection
sudo amazon-linux-extras install postgresql10