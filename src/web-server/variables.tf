variable "region" {
  type        = string
  description = "Region where the AWS resources are provisioned"
}

variable "environment" {
  type        = string
  description = "Environment: Prod or Dev"
}

variable "max_size" {
  type        = string
  description = "Maximus amount of instances in the web-server ASG"
}

variable "min_size" {
  type        = string
  description = "Minimum amount of instances in the web-server ASG"
}

variable "desired_capacity" {
  type        = string
  description = "Desired amount of instances in the web-server ASG"
}