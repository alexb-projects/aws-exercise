data "template_file" "this" {
  template = file("./user-data.sh")

  vars = {
    greeting_message = "Alex Bespalov"
  }
}