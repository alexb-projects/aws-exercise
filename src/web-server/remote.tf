data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket  = "aws-excersise-project-alex-b-terraform-state"
    key     = "${var.environment}/vpc/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}