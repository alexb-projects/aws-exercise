terraform {
  required_version = "~> 1.3.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.32.0"
    }
  }

  backend "s3" {
  }
}

provider "aws" {
  region = var.region
}

locals {
  resource_name = basename(path.cwd)

  tags = {
    name  = local.resource_name
    env   = var.environment
    scope = "${var.environment}-${local.resource_name}"
  }
}

resource "aws_security_group" "this" {
  name        = "${local.resource_name}-sg"
  description = "Allow inbound / outbound traffic to the web-server"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc.id
  tags        = local.tags
}

resource "aws_security_group_rule" "http" {
  description = "Allow HTTP inbound traffic"
  type        = "ingress"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  from_port   = "80"
  to_port     = "80"

  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "ssh" {

  description = "Allow traffic for SSH connection"
  type        = "ingress"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  from_port   = "22"
  to_port     = "22"

  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "egress_https" {
  description       = "Allow HTTPS outboud traffic. Required to download packages."
  type              = "egress"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 443
  to_port           = 443
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "egress_db" {
  description       = "Allow accessing the database in the private subnet"
  type              = "egress"
  protocol          = "tcp"
  cidr_blocks       = [data.terraform_remote_state.vpc.outputs.vpc.cidr_block]
  from_port         = 5432
  to_port           = 5432
  security_group_id = aws_security_group.this.id
}


resource "aws_launch_configuration" "this" {
  name_prefix     = "${var.environment}-${local.resource_name}"
  instance_type   = "t2.micro"
  image_id        = "ami-026b57f3c383c2eec"
  security_groups = [aws_security_group.this.id]
  user_data       = data.template_file.this.rendered

  root_block_device {
    volume_size           = "16"
    volume_type           = "gp3"
    delete_on_termination = true
    encrypted             = true
  }

  ebs_block_device {
    device_name           = "/dev/sda2"
    volume_size           = "8"
    volume_type           = "gp3"
    delete_on_termination = true
    encrypted             = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  name                      = "${var.environment}-${local.resource_name}-asg"
  launch_configuration      = aws_launch_configuration.this.name
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  health_check_type         = "EC2"
  health_check_grace_period = 600
  force_delete              = false
  vpc_zone_identifier       = data.terraform_remote_state.vpc.outputs.subnets.public
  termination_policies      = ["OldestLaunchConfiguration", "Default"]

  depends_on = [aws_launch_configuration.this]

  lifecycle {
    create_before_destroy = true
  }
}
