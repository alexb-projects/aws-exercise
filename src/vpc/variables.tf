variable "region" {
  type        = string
  description = "Region where the AWS resources are provisioned"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC cire range"
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "Private subtent cidr ranges of the main VPC"
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public subtent cidr ranges of the main VPC"
}

variable "availability_zones" {
  type        = list(string)
  description = "Subnet availability zones VPC"
}