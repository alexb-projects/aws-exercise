output "subnets" {
  value = {
    public  = module.vpc.public_subnets
    private = module.vpc.database_subnets
  }
}

output "vpc" {
  value = {
    id         = module.vpc.vpc_id
    cidr_block = module.vpc.vpc_cidr_block
  }
}

output "db_subnet_group" {
  value = module.vpc.database_subnet_group
}
