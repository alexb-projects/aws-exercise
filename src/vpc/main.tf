terraform {
  required_version = "~> 1.3.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.32.0"
    }
  }

  backend "s3" {
  }
}

provider "aws" {
  region = var.region
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"

  name                               = "main-vpc"
  cidr                               = var.vpc_cidr
  azs                                = var.availability_zones
  public_subnets                     = var.public_subnet_cidrs
  database_subnets                   = var.private_subnet_cidrs
  enable_nat_gateway                 = true
  single_nat_gateway                 = true
  enable_dns_hostnames               = true
  create_database_subnet_group       = true
  create_database_subnet_route_table = true

  public_subnet_tags = {
    "type" = "public"
  }

  database_subnet_tags = {
    "type" = "private"
  }
}